#include <boost/process.hpp>
#include <gtest/gtest.h>
#include <filesystem>
#include <set>
#include <string>
#include <vector>

namespace bp = boost::process;

namespace {
class ConsoleIO {
public:
    bp::opstream consoleIn;
    bp::ipstream consoleOut;
    bp::child console;

    ConsoleIO() : console("../Debug/ConsoleEditor.exe", bp::std_out > consoleOut, bp::std_in < consoleIn) {
        flushConsoleOut();
    }

    void flushConsoleOut() {
        std::string s;
        while (consoleOut.peek() != '>')
            std::getline(consoleOut, s);
    }

    void executeCommand(const std::string & command, int repetitions = 1) {
        for (; repetitions > 0; repetitions--) {
            consoleOut.ignore(2);
            consoleIn << command << std::endl;
        }
    }

    void verifyOutput(const std::string & command, const std::multiset<std::string> & expectedOutput = {}) {
        executeCommand(command);
        std::multiset<std::string> consoleOutput;
        std::string consoleLine;
        while (consoleOut.peek() != '>') {
            std::getline(consoleOut, consoleLine);
            consoleLine.pop_back();
            consoleOutput.insert(consoleLine);
        }
        EXPECT_EQ(consoleOutput, expectedOutput);
    }
};
}

TEST(Console, GraphTest) {
    ConsoleIO io;
    std::vector<std::multiset<std::string>> lists;

    /* Build test graph */
    io.verifyOutput("addnode 1 2 oslo");
    io.verifyOutput("addnode 2 3 stockholm");
    io.verifyOutput("addnode 3 4 copenhagen");
    io.verifyOutput("addnode 4 5 helsinki");
    io.verifyOutput("addedge 0 1 red");
    io.verifyOutput("addedge 1 2 blue");
    io.verifyOutput("addedge 2 3 brown");
    io.verifyOutput("addedge 3 1 green");
    
    /* Verify state */
    io.verifyOutput("printnode 0", { "Name: oslo   x: 1   y: 2", });
    io.verifyOutput("printedge 2", { "Name: brown   Links: 2 and 3", });
    lists.push_back({
        "ID: 0   Name: oslo   x: 1   y: 2",
        "ID: 1   Name: stockholm   x: 2   y: 3",
        "ID: 2   Name: copenhagen   x: 3   y: 4",
        "ID: 3   Name: helsinki   x: 4   y: 5",
    });
    io.verifyOutput("listnodes", lists[0]);
    lists.push_back({
        "ID: 3   Name: green   Links: 3 and 1",
        "ID: 1   Name: blue   Links: 1 and 2",
        "ID: 0   Name: red   Links: 0 and 1",
        "ID: 2   Name: brown   Links: 2 and 3",
    });
    io.verifyOutput("listedges", lists[1]);
    io.verifyOutput("listedges 3", { "Edges connected to node 3: 2 3", });

    /* Modify the graph a little */
    io.verifyOutput("deletenode 1");
    io.verifyOutput("movenode 0 19 -5");
    io.verifyOutput("moveedge 2 3 0");
    lists.push_back({
        "ID: 0   Name: oslo   x: 19   y: -5",
        "ID: 2   Name: copenhagen   x: 3   y: 4",
        "ID: 3   Name: helsinki   x: 4   y: 5",
    });
    io.verifyOutput("listnodes", lists[2]);
    io.verifyOutput("listedges", { "ID: 2   Name: brown   Links: 3 and 0", });
    io.verifyOutput("listedges 3", { "Edges connected to node 3: 2", });

    /* Undo changes */
    io.executeCommand("undo", 3);
    io.verifyOutput("listnodes", lists[0]);
    io.verifyOutput("listedges", lists[1]);
    io.verifyOutput("listedges 1", { "Edges connected to node 1: 0 1 3", });
}

TEST(Console, DiskIOTest) {
    ConsoleIO io;
    std::vector<std::multiset<std::string>> lists;

    io.verifyOutput("addnode 0 0 a");
    io.verifyOutput("addnode 1 1 b");
    io.verifyOutput("addnode 2 2 c");
    io.verifyOutput("addedge 0 1 ab");
    io.verifyOutput("addedge 1 2 bc");

    lists.push_back({
        "ID: 0   Name: a   x: 0   y: 0",
        "ID: 1   Name: b   x: 1   y: 1",
        "ID: 2   Name: c   x: 2   y: 2",
    });
    lists.push_back({
        "ID: 0   Name: ab   Links: 0 and 1",
        "ID: 1   Name: bc   Links: 1 and 2",
    });
    io.verifyOutput("listnodes", lists[0]);
    io.verifyOutput("listedges", lists[1]);

    std::string tempFile = std::filesystem::temp_directory_path().string() + "model0.json";
    io.verifyOutput("save " + tempFile);
    io.verifyOutput("newproject");
    io.verifyOutput("listnodes");
    io.verifyOutput("listedges");
    io.verifyOutput("addnode 3 5 foo");
    io.verifyOutput("listnodes", { "ID: 0   Name: foo   x: 3   y: 5" });

    io.verifyOutput("load " + tempFile);
    io.verifyOutput("printlocation", { "ID: 0   Name: untitled" });
    io.verifyOutput("listnodes", lists[0]);
    io.verifyOutput("listedges", lists[1]);
}

TEST(Console, LoadNonExistingFile) {
    ConsoleIO io;
    io.verifyOutput("load nonexisting", { "Could not open file" });
}

#pragma once
#include "movementgraph.h"
#include "object.h"
#include <memory>
#include <string>

using LocationId = unsigned int;
class Exit;

class Location {
public:
    Location()
        : movementGraph_(std::make_shared<const MovementGraph>())
    { ; }
    const MovementGraph & graph() const { return *movementGraph_; }
    const std::string & sequence(int index) { return *(*sequences)[index]; }
    void replaceGraph(MovementGraph && graph) {
        movementGraph_ = std::make_shared<const MovementGraph>(graph);
    }
private:
    std::shared_ptr<const MovementGraph> movementGraph_;
    std::shared_ptr<const std::vector<std::shared_ptr<const std::string>>> sequences;
    std::shared_ptr<const std::unordered_map<std::string, std::shared_ptr<const Object>>> objects;
};

class Exit {
public:
    void setSource(NodeId node) {
        sourceNode_ = node;
    }
    void setDestination(LocationId location, NodeId node) {
        destinationLocation_ = location;
        destinationNode_ = node;
    }
private:
    NodeId sourceNode_;
    LocationId destinationLocation_;
    NodeId destinationNode_;
};

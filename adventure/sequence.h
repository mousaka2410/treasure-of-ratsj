#pragma once
#include "location.h"
#include <sstream>

class AnimPlayer {

};


class MissingArgumentException : public std::exception {};

class ConvertToIntegerException : public std::exception {};

void readNumber(std::istream & stream, int & val) {
    if (stream.eof())
        throw MissingArgumentException();
    stream >> val;
    if (stream.fail())
        throw ConvertToIntegerException();
}

void readString(std::istream & stream, std::string & s) {
    if (stream.eof())
        throw MissingArgumentException();
    stream >> s;
}

class SequencePlayer {
public:
    SequencePlayer(Location & location, int sequenceNumber)
        : location_(location)
        , sequence_(location_.sequence(sequenceNumber))
        , duration_(0)
    { ; }

    // Returns true when the sequence is done
    bool update() {
        if (duration_)
            return false;
        std::string line;
        while (!sequence_.eof() && duration_ == 0) {
            std::getline(sequence_, line);
            std::istringstream stream(line);
            parseLine(stream);
        }
        return duration_ == 0;
    }
private:
    void parseLine(std::istream & line) {
        std::string word1;
        readString(line, word1);
        if (word1 == "wait") {
            readNumber(line, duration_);
        } else {

        }

    }

    Location & location_;

    std::stringstream sequence_;

    // duration > 0: remaining time until next step.
    // duration < 0: wait until animation of object -(1 + duration) finishes
    int duration_;
};

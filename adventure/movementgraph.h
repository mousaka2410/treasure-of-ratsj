#pragma once
#include "graph.h"

struct Node {
    Node() = default;
    Node(int x, int y, std::string name) : x(x), y(y), name(name) { ; }
    int x, y;
    std::string name;
};

struct Edge {
    Edge() = default;
    Edge(std::string name) : name(name) { ; }
    std::string name;
};

using MovementGraph = Graph<Node, Edge>;
using NodeIterator = MovementGraph::NodeConstIterator;
using EdgeIterator = MovementGraph::EdgeConstIterator;

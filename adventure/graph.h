#pragma once
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <stdexcept>

using NodeId = unsigned int;
using EdgeId = unsigned int;

template <class NodeData_, class EdgeData_>
class Graph {
public:
    struct Node;
    struct Edge;

    using NodeData = NodeData_;
    using EdgeData = EdgeData_;
    using NodeIterator = typename std::unordered_map<NodeId, Node>::iterator;
    using EdgeIterator = typename std::unordered_map<EdgeId, Edge>::iterator;
    using NodeConstIterator = typename std::unordered_map<NodeId, Node>::const_iterator;
    using EdgeConstIterator = typename std::unordered_map<EdgeId, Edge>::const_iterator;

public:
    class NodeNeighbours : private std::vector<EdgeId> {
    private:
        using container_type = std::vector<EdgeId>;
    public:
        friend class Graph;
        using const_iterator = container_type::const_iterator;
        using container_type::cbegin;
        using container_type::cend;
        using container_type::size;
        using container_type::empty;
    };

    struct Node {
        Node() = default;
        Node(NodeData data)
            : data(data) { ; }
        NodeData data;
        NodeNeighbours neighbours;
    };

    struct Edge {
        Edge() = default;
        Edge(NodeId a, NodeId b, EdgeData data)
            : a(a), b(b), data(data) { ; }
        NodeId a, b;
        EdgeData data;
    };

public:
    explicit Graph(std::size_t hashSize = 32) {
        nodes_.reserve(hashSize);
        edges_.reserve(hashSize);
    }

    NodeId addNode(NodeData data = NodeData{});
    EdgeId addEdge(NodeId a, NodeId b, EdgeData data = EdgeData{});
    void deleteNode(NodeId a);
    void deleteEdge(EdgeId e);
    void insertNode(NodeId id, Node node);
    void insertEdge(EdgeId id, Edge edge);

    Node & node(NodeId a);
    const Node & node(NodeId a) const;
    Edge & edge(EdgeId e);
    const Edge & edge(EdgeId e) const;
    void moveEdge(EdgeId e, NodeId a, NodeId b);

    NodeIterator nodeBegin();
    NodeIterator nodeEnd();
    EdgeIterator edgeBegin();
    EdgeIterator edgeEnd();
    NodeConstIterator cNodeBegin() const;
    NodeConstIterator cNodeEnd() const;
    EdgeConstIterator cEdgeBegin() const;
    EdgeConstIterator cEdgeEnd() const;

private:
    std::unordered_map<NodeId, Node> nodes_;
    std::unordered_map<EdgeId, Edge> edges_;
    NodeId nextNodeId = 0;
    EdgeId nextEdgeId = 0;

    static void deleteNeighbour(NodeNeighbours & neighbours, EdgeId e);
};

template<class N, class E>
inline NodeId Graph<N, E>::addNode(NodeData data)
{
    nodes_.emplace(nextNodeId, Node(data));
    return nextNodeId++;
}

template<class N, class E>
inline EdgeId Graph<N, E>::addEdge(NodeId a, NodeId b, EdgeData data)
{
    auto foundA = nodes_.find(a);
    auto foundB = nodes_.find(b);
    if (foundA == nodes_.end() || foundB == nodes_.end())
        throw std::invalid_argument("Invalid NodeId");
    edges_.emplace(nextEdgeId, Edge(a, b, data));
    foundA->second.neighbours.push_back(nextEdgeId);
    foundB->second.neighbours.push_back(nextEdgeId);
    return nextEdgeId++;
}

template<class N, class E>
inline void Graph<N, E>::deleteNode(NodeId a)
{
    auto found = nodes_.find(a);
    if (found == nodes_.end())
        throw std::invalid_argument("Invalid NodeId");
    auto & neighbours = found->second.neighbours;
    while (!neighbours.empty()) {
        deleteEdge(neighbours.front());
    }
    nodes_.erase(found);
}

template<class NodeData_, class EdgeData_>
inline void Graph<NodeData_, EdgeData_>::deleteNeighbour(NodeNeighbours & neighbours, EdgeId e)
{
    auto found = std::find(neighbours.begin(), neighbours.end(), e);
    *found = neighbours.back();
    neighbours.pop_back();
}

template<class N, class E>
inline void Graph<N, E>::deleteEdge(EdgeId e)
{
    auto found = edges_.find(e);
    if (found == edges_.end())
        throw std::invalid_argument("Invalid EdgeId");
    Edge & edge = found->second;
    deleteNeighbour(nodes_[edge.a].neighbours, e);
    deleteNeighbour(nodes_[edge.b].neighbours, e);
    edges_.erase(found);
}

template<class N, class E>
inline void Graph<N, E>::insertNode(NodeId id, Node node) {
    auto success = nodes_.emplace(id, node);
    if (!success.second)
        throw std::invalid_argument("Invalid NodeId");
    if (id >= nextNodeId)
        nextNodeId = id + 1;
}

template<class N, class E>
inline void Graph<N, E>::insertEdge(EdgeId id, Edge edge) {
    auto foundA = nodes_.find(edge.a);
    auto foundB = nodes_.find(edge.b);
    if (foundA == nodes_.end() || foundB == nodes_.end())
        throw std::invalid_argument("Invalid NodeId");
    auto success = edges_.emplace(id, edge);
    if (!success.second)
        throw std::invalid_argument("Invalid NodeId");
    foundA->second.neighbours.push_back(id);
    foundB->second.neighbours.push_back(id);
    if (id >= nextEdgeId)
        nextEdgeId = id + 1;
}

template<class N, class E>
inline typename Graph<N, E>::Node & Graph<N, E>::node(NodeId a)
{
    auto found = nodes_.find(a);
    if (found == nodes_.end())
        throw std::invalid_argument("Invalid NodeId");
    return found->second;
}

template<class N, class E>
inline const typename Graph<N, E>::Node & Graph<N, E>::node(NodeId a) const
{
    auto found = nodes_.find(a);
    if (found == nodes_.end())
        throw std::invalid_argument("Invalid NodeId");
    return found->second;
}

template<class N, class E>
inline typename Graph<N, E>::Edge & Graph<N, E>::edge(EdgeId e)
{
    auto found = edges_.find(e);
    if (found == edges_.end())
        throw std::invalid_argument("Invalid EdgeId");
    return found->second;
}

template<class N, class E>
inline const typename Graph<N, E>::Edge & Graph<N, E>::edge(EdgeId e) const
{
    auto found = edges_.find(e);
    if (found == edges_.end())
        throw std::invalid_argument("Invalid EdgeId");
    return found->second;
}

template<class N, class E>
inline void Graph<N, E>::moveEdge(EdgeId e, NodeId a, NodeId b)
{
    auto foundE = edges_.find(e);
    auto foundA = nodes_.find(a);
    auto foundB = nodes_.find(b);
    if (foundE == edges_.end())
        throw std::invalid_argument("Invalid EdgeId");
    else if (foundA == nodes_.end() || foundB == nodes_.end())
        throw std::invalid_argument("Invalid NodeId");
    Edge & edge = foundE->second;
    deleteNeighbour(nodes_[edge.a].neighbours, e);
    deleteNeighbour(nodes_[edge.b].neighbours, e);
    edge.a = a;
    edge.b = b;
    foundA->second.neighbours.push_back(e);
    foundB->second.neighbours.push_back(e);
}

template<class N, class E>
inline typename Graph<N, E>::NodeIterator Graph<N, E>::nodeBegin() {
    return NodeIterator(nodes_.begin());
}

template<class N, class E>
inline typename Graph<N, E>::NodeIterator Graph<N, E>::nodeEnd() {
    return NodeIterator(nodes_.end());
}

template<class N, class E>
inline typename Graph<N, E>::EdgeIterator Graph<N, E>::edgeBegin() {
    return EdgeIterator(edges_.begin());
}

template<class N, class E>
inline typename Graph<N, E>::EdgeIterator Graph<N, E>::edgeEnd() {
    return EdgeIterator(edges_.end());
}

template<class N, class E>
inline typename Graph<N, E>::NodeConstIterator Graph<N, E>::cNodeBegin() const {
    return NodeConstIterator(nodes_.cbegin());
}

template<class N, class E>
inline typename Graph<N, E>::NodeConstIterator Graph<N, E>::cNodeEnd() const {
    return NodeConstIterator(nodes_.cend());
}

template<class N, class E>
inline typename Graph<N, E>::EdgeConstIterator Graph<N, E>::cEdgeBegin() const {
    return EdgeConstIterator(edges_.cbegin());
}

template<class N, class E>
inline typename Graph<N, E>::EdgeConstIterator Graph<N, E>::cEdgeEnd() const {
    return EdgeConstIterator(edges_.cend());
}

#pragma once
#include "../editoractions/editormodel.h"
#include <string>

class GameFileIO {
public:
    static void save(const std::string & fileName, EditorModel & currentModel);
    static EditorModel load(const std::string & fileName);
};

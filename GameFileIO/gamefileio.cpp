#include "gamefileio.h"
#include "../editoractions/editormodel.h"
#include <nlohmann/json.hpp>
#include <fstream>

using json = nlohmann::json;


// Save

void to_json(json & j, const MovementGraph::Node & node) {
    const Node & data = node.data;
    j = json{ {"x", data.x}, {"y", data.y}, {"name", data.name} };
}

void to_json(json & j, const MovementGraph::Edge & edge) {
    j = json{ {"a", edge.a}, {"b", edge.b}, {"name", edge.data.name} };
}

void to_json(json & j, const MovementGraph & graph) {
    for (auto it = graph.cNodeBegin(); it != graph.cNodeEnd(); ++it)
        j["nodes"].push_back({ {"id", it->first}, {"data", it->second} });
    for (auto it = graph.cEdgeBegin(); it != graph.cEdgeEnd(); ++it)
        j["edges"].push_back({ {"id", it->first}, {"data", it->second} });
}

void to_json(json & j, EditorLocation & location) {
    j["name"] = location.name();
    j["movement_graph"] = json(location.graph());
}

void GameFileIO::save(const std::string & fileName, EditorModel & currentModel) {
    json jModel;
    LocationCollection & locations = currentModel.locations();
    for (const auto & pair : locations) {
        jModel["locations"].push_back({ {"id", pair.first} });
        to_json(jModel["locations"].back(), pair.second.current());
    }
    std::ofstream fout(fileName);
    fout << jModel.dump(2) << std::flush;
}


// Load

void from_json(const json & j, MovementGraph::Node & node) {
    Node & data = node.data;
    j.at("x").get_to(data.x);
    j.at("y").get_to(data.y);
    j.at("name").get_to(data.name);
}

void from_json(const json & j, MovementGraph::Edge & edge) {
    j.at("a").get_to(edge.a);
    j.at("b").get_to(edge.b);
    j.at("name").get_to(edge.data.name);
}

void from_json(const json & j, MovementGraph & graph) {
    const json & jNodes = j.at("nodes");
    for (const json & jNode : jNodes) {
        NodeId id;
        MovementGraph::Node node;
        jNode.at("id").get_to(id);
        jNode.at("data").get_to(node);
        graph.insertNode(id, node);
    }
    const json & jEdges = j.at("edges");
    for (const json & jEdge : jEdges) {
        EdgeId id;
        MovementGraph::Edge edge;
        jEdge.at("id").get_to(id);
        jEdge.at("data").get_to(edge);
        graph.insertEdge(id, edge);
    }
}

void from_json(const json & j, std::pair<LocationId, EditorLocation> & pair) {
    EditorLocation & location = pair.second;
    std::string name;
    j.at("id").get_to(pair.first);
    j.at("name").get_to(name);
    location.rename(name);

    MovementGraph graph;
    j.at("movement_graph").get_to(graph);
    location.replaceGraph(std::move(graph));
}

EditorModel GameFileIO::load(const std::string & fileName) {
    EditorModel model;
    LocationCollection & locations = model.locations();
    json jModel;
    std::ifstream fin(fileName);
    if (fin.fail())
        throw std::runtime_error("Could not open file");
    fin >> jModel;
    json & jLocations = jModel.at("locations");
    for (const json & jLocation : jLocations) {
        std::pair<LocationId, EditorLocation> pair;
        jLocation.get_to(pair);
        locations.insertLocation(pair.first, std::move(pair.second));
        locations.current().rename(pair.second.name());
    }
    return model;
}

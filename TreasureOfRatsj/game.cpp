#include "game.h"

const Uint8* keystates = SDL_GetKeyboardState(NULL);

void Game::update(milliseconds dt) {
    Dot & dot = gameState_.dot;
    int vx = keystates[SDL_SCANCODE_RIGHT] - keystates[SDL_SCANCODE_LEFT];
    int vy = keystates[SDL_SCANCODE_DOWN] - keystates[SDL_SCANCODE_UP];
    dot.x += vx * dt.count() / 20;
    dot.y += vy * dt.count() / 20;
}

void Game::gameloop() {
    using namespace std::chrono_literals;
    using std::chrono::steady_clock;
    SDL_Event sdlEvent;
    Dot & dot = gameState_.dot;
    auto prevTime = steady_clock::now();
    auto currentTime = steady_clock::now();
    bool running = true;
    while (running) {
        SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 255);
        SDL_RenderClear(renderer_);
        SDL_SetRenderDrawColor(renderer_, 255, 0, 0, 255);
        SDL_RenderDrawPoint(renderer_, dot.x, dot.y);
        SDL_RenderPresent(renderer_);
        while (currentTime - prevTime < 20ms) {
            currentTime = steady_clock::now();
        }
        auto dt = currentTime - prevTime;
        update(std::chrono::duration_cast<milliseconds>(dt));
        prevTime = currentTime;

        while (SDL_PollEvent(&sdlEvent)) {
            switch (sdlEvent.type) {
            case SDL_KEYDOWN:
                switch (sdlEvent.key.keysym.sym) {
                case SDLK_ESCAPE:
                    running = false;
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }
    }
}

#pragma once
#include <SDL2/SDL.h>
#include <stdexcept>
#include <memory>
#include <chrono>

class SdlWindow {
public:
    SdlWindow(SDL_Window * window)
        : ptr_(window)
    {
        if (window == nullptr)
            throw std::runtime_error(std::string("SdlWindow: ") + SDL_GetError());
    }

    operator SDL_Window*() const { return ptr_.get(); }

private:
    struct SdlWindowDestroyer {
        void operator()(SDL_Window * window) {
            SDL_DestroyWindow(window);
        }
    };

    std::unique_ptr<SDL_Window, SdlWindowDestroyer> ptr_;
};

class SdlRenderer {
public:
    SdlRenderer(SDL_Renderer * renderer)
        : ptr_(renderer)
    {
        if (renderer == nullptr)
            throw std::runtime_error(std::string("SdlRenderer: ") + SDL_GetError());
    }

    operator SDL_Renderer*() const { return ptr_.get(); }

private:
    struct SdlRendererDestroyer {
        void operator()(SDL_Renderer * renderer) {
            SDL_DestroyRenderer(renderer);
        }
    };

    std::unique_ptr<SDL_Renderer, SdlRendererDestroyer> ptr_;
};

enum class GameMode {
    NORMAL,
    DIALOG,
};

struct Dot {
    Dot(int x, int y) : x(x), y(y) { ; }
    int x, y;
};

struct GameState {
    GameMode mode;
    Dot dot = Dot(100, 100);
};

class Game {
public:
    using milliseconds = std::chrono::duration<int, std::milli>;
    Game(SdlWindow && window, SdlRenderer && renderer) :
        window_(std::move(window)), renderer_(std::move(renderer)) { ; }
    void gameloop();

    void update(milliseconds dt);

private:
    SdlWindow window_;
    SdlRenderer renderer_;
    GameState gameState_;
};

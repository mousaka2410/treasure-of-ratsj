#include "game.h"

#include <cstdlib>
#include <iostream>
#include <string>

Game createGame() {
    SDL_Init(SDL_INIT_VIDEO);
    std::atexit(SDL_Quit);
    SdlWindow window(
        SDL_CreateWindow(
            "Treasure of Ratsj",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            640,
            480,
            SDL_WINDOW_FULLSCREEN
        )
    );
    SdlRenderer renderer(SDL_CreateRenderer(window, -1, 0));
    return Game(std::move(window), std::move(renderer));
}

void treasureOfRatsj() {
    Game game = createGame();
    game.gameloop();
}

int main(int argc, char *argv[]) {
    try {
        treasureOfRatsj();
    } catch(std::exception & ex) {
        std::cerr << ex.what() << std::endl;
    }
    return 0;
}
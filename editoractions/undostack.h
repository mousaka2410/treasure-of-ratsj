#pragma once
#include <vector>

template <class T>
class UndoStack {
public:
    UndoStack()
        : undoStack_{ T() }
        , current_(undoStack_.begin())
    { ; }
    UndoStack(UndoStack && s) noexcept
        : undoStack_(std::move(s.undoStack_))
        , current_(s.current_)
    { ; }
    UndoStack & operator=(UndoStack && s) noexcept {
        undoStack_ = std::move(s.undoStack_);
        current_ = s.current_;
    }
    ~UndoStack() = default;
    explicit UndoStack(T t) : undoStack_{ t }, current_(undoStack_.begin()) { ; }
    T & current() const { return *current_; }
    void undo() {
        if (current_ != undoStack_.begin())
            --current_;
    }
    void redo() {
        ++current_;
        if (current_ == undoStack_.end()) --current_;
    }
    void commit() {
        undoStack_.erase(current_ + 1, undoStack_.end());
        undoStack_.push_back(undoStack_.back());
        current_ = undoStack_.end() - 1;
    }
private:
    std::vector<T> undoStack_;
    typename std::vector<T>::iterator current_;
};

#pragma once
#include "editorlocation.h"
#include "undostack.h"
#include <unordered_map>

class LocationCollection : private std::unordered_map<LocationId, UndoStack<EditorLocation>> {
public:
    LocationCollection() {
        reserve(64);
        nextLocationId_ = 0;
    }
    LocationId addLocation(std::string name = "untitled") {
        emplace(nextLocationId_, EditorLocation(name));
        current_ = find(nextLocationId_);
        return nextLocationId_++;
    }
    void insertLocation(LocationId id, EditorLocation && location) {
        auto success = emplace(id, EditorLocation(location));
        if (!success.second)
            throw std::invalid_argument("Invalid LocationId");
        current_ = find(id);
        if (nextLocationId_ <= id)
            nextLocationId_ = id + 1;
    }
    void selectLocation(LocationId id) {
        auto found = find(id);
        if (found == end())
            throw std::invalid_argument("Invalid LocationId");
        current_ = found;
    }
    LocationId currentId() {
        if (empty())
            throw std::out_of_range("No locations found");
        return current_->first;
    }
    EditorLocation & current() {
        if (empty())
            throw std::out_of_range("No locations found");
        return current_->second.current();
    }
    void undo() { current_->second.undo(); }
    void redo() { current_->second.redo(); }
    void commit() { current_->second.commit(); }

    using LocationMap = std::unordered_map<LocationId, UndoStack<EditorLocation>>;
    using LocationMap::begin;
    using LocationMap::end;

private:
    LocationId nextLocationId_;
    LocationMap::iterator current_;
};

#include "stdafx.h"
#include "editoractions.h"
#include "editormodel.h"
#include "../GameFileIO/gamefileio.h"
#include <memory>

EditorActions::EditorActions()
    : model_(std::make_unique<EditorModel>())
{ ; }
EditorActions::~EditorActions() = default;

void EditorActions::undo()
{
    model_->locations().undo();
}

void EditorActions::redo()
{
    model_->locations().redo();
}

void EditorActions::newProject() {
    model_ = std::make_unique<EditorModel>();
}

void EditorActions::save(const std::string & file)
{
    GameFileIO::save(file, *model_);
}

void EditorActions::load(const std::string & file)
{
    model_ = std::make_unique<EditorModel>(GameFileIO::load(file));
}


/* Loctaion modifiers */

LocationId EditorActions::addLocation(std::string name) {
    return model_->locations().addLocation(name);
}

LocationId EditorActions::currentLocationId() {
    return model_->locations().currentId();
}

void EditorActions::selectLocation(LocationId id) {
    model_->locations().selectLocation(id);
}

void EditorActions::renameLocation(std::string name) {
    model_->locations().current().rename(name);
}

std::string EditorActions::locationName() {
    return model_->locations().current().name();
}


/* Graph modifiers */

NodeId EditorActions::addNode(int x, int y, std::string s)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    NodeId id = graph.addNode(Node(x, y, s));
    location.replaceGraph(std::move(graph));
    return id;
}

EdgeId EditorActions::addEdge(NodeId a, NodeId b, std::string s)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    EdgeId id = graph.addEdge(a, b, Edge(s));
    location.replaceGraph(std::move(graph));
    return id;
}

void EditorActions::deleteNode(NodeId a)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    graph.deleteNode(a);
    location.replaceGraph(std::move(graph));
}

void EditorActions::deleteEdge(EdgeId e)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    graph.deleteEdge(e);
    location.replaceGraph(std::move(graph));
}

void EditorActions::moveNode(NodeId a, int x, int y)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    MovementGraph::Node & node = graph.node(a);
    node.data.x = x;
    node.data.y = y;
    location.replaceGraph(std::move(graph));
}

void EditorActions::moveEdge(EdgeId e, NodeId a, NodeId b)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    graph.moveEdge(e, a, b);
    location.replaceGraph(std::move(graph));
}

void EditorActions::renameNode(NodeId a, std::string s)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    MovementGraph::Node & node = graph.node(a);
    node.data.name = s;
    location.replaceGraph(std::move(graph));
}

void EditorActions::renameEdge(EdgeId e, std::string s)
{
    model_->locations().commit();
    EditorLocation & location = model_->locations().current();
    MovementGraph graph = location.graph();
    MovementGraph::Edge & edge = graph.edge(e);
    edge.data.name = s;
    location.replaceGraph(std::move(graph));
}

const MovementGraph::Node & EditorActions::node(NodeId a) {
    const MovementGraph & graph = model_->locations().current().graph();
    return graph.node(a);
}

const MovementGraph::Edge & EditorActions::edge(EdgeId e) {
    const MovementGraph & graph = model_->locations().current().graph();
    return graph.edge(e);
}

NodeIterator EditorActions::nodeBegin() {
    const MovementGraph & graph = model_->locations().current().graph();
    return graph.cNodeBegin();
}

NodeIterator EditorActions::nodeEnd() {
    const MovementGraph & graph = model_->locations().current().graph();
    return graph.cNodeEnd();
}

EdgeIterator EditorActions::edgeBegin() {
    const MovementGraph & graph = model_->locations().current().graph();
    return graph.cEdgeBegin();
}

EdgeIterator EditorActions::edgeEnd() {
    const MovementGraph & graph = model_->locations().current().graph();
    return graph.cEdgeEnd();
}

#pragma once
#include "locationcollection.h"

class EditorModel {
public:
    LocationCollection & locations() {
        return locations_;
    }
private:
    LocationCollection locations_;
};

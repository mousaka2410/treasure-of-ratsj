#pragma once
#include "../adventure/movementgraph.h"
#include "../adventure/object.h"
#include <memory>
#include <string>

using LocationId = unsigned int;

class EditorLocation {
public:
    EditorLocation()
        : name_(std::make_shared<std::string>("untitled"))
        , movementGraph_(std::make_shared<const MovementGraph>())
    { ; }
    EditorLocation(const std::string & name)
        : name_(std::make_shared<std::string>(name))
        , movementGraph_(std::make_shared<const MovementGraph>())
    { ; }
    const MovementGraph & graph() const { return *movementGraph_; }
    const std::string & sequence(int index) { return *(*sequences)[index]; }
    void replaceGraph(MovementGraph && graph) {
        movementGraph_ = std::make_shared<const MovementGraph>(graph);
    }
    const std::string & name() const { return *name_; }
    void rename(std::string name) { *name_ = name; }
private:
    std::shared_ptr<std::string> name_;
    std::shared_ptr<const MovementGraph> movementGraph_;
    std::shared_ptr<const std::vector<std::shared_ptr<const std::string>>> sequences;
    std::shared_ptr<const std::unordered_map<std::string, std::shared_ptr<const Object>>> objects;
};

#pragma once
#include "editorlocation.h"
#include <memory>
#include <string>

class EditorModel;

class EditorActions {
public:
    EditorActions();
    ~EditorActions();
    void undo();
    void redo();
    void newProject();
    void save(const std::string & file);
    void load(const std::string & file);

    // Location modifiers
    LocationId addLocation(std::string name = "untitled");
    LocationId currentLocationId();
    void selectLocation(LocationId id);
    void renameLocation(std::string name);
    std::string locationName();

    // Graph modifiers
    NodeId addNode(int x, int y, std::string s = "");
    EdgeId addEdge(NodeId a, NodeId b, std::string s = "");
    void deleteNode(NodeId a);
    void deleteEdge(EdgeId e);
    void moveNode(NodeId a, int x, int y);
    void moveEdge(EdgeId e, NodeId a, NodeId b);
    void renameNode(NodeId a, std::string s);
    void renameEdge(EdgeId e, std::string s);

    const MovementGraph::Node & node(NodeId a);
    const MovementGraph::Edge & edge(EdgeId e);
    NodeIterator nodeBegin();
    NodeIterator nodeEnd();
    EdgeIterator edgeBegin();
    EdgeIterator edgeEnd();

private:
    std::unique_ptr<EditorModel> model_;
};

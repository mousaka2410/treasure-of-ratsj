#include "../adventure/graph.h"
#include <gtest/gtest.h>
#include <vector>

struct NodeData {
    NodeData() = default;
    NodeData(int x, int y, int weight)
        : x(x), y(y), weight(weight) { ; }
    int x, y, weight;
};

struct EdgeData {
    EdgeData() = default;
    EdgeData(int weight)
        : weight(weight) { ; }
    int weight;
};

TEST(Graph, InterfaceTest) {
    /* Test graph:
        a-b-c
          \ /
           d
    */

    // Add nodes
    Graph<NodeData, EdgeData> graph;
    NodeId a = graph.addNode(NodeData(11, 12, 3));
    EXPECT_EQ(a, 0);
    NodeId b = graph.addNode();
    EXPECT_EQ(b, 1);
    NodeData & ndata1 = graph.node(a).data;
    EXPECT_EQ(ndata1.x, 11);
    EXPECT_EQ(ndata1.y, 12);
    EXPECT_EQ(ndata1.weight, 3);
    NodeData & ndata2 = graph.node(b).data;
    EXPECT_EQ(ndata2.x, 0);
    EXPECT_EQ(ndata2.y, 0);
    EXPECT_EQ(ndata2.weight, 0);
    NodeId c = graph.addNode();
    NodeId d = graph.addNode();

    // Add edges
    EdgeId e = graph.addEdge(a, b, EdgeData(5));
    EXPECT_EQ(e, 0);
    EdgeId f = graph.addEdge(b, c);
    EXPECT_EQ(f, 1);
    EdgeData & edata1 = graph.edge(e).data;
    EXPECT_EQ(edata1.weight, 5);
    EdgeData & edata2 = graph.edge(f).data;
    EXPECT_EQ(edata2.weight, 0);
    EdgeId g = graph.addEdge(c, d);
    EdgeId h = graph.addEdge(b, d);
    Graph<NodeData, EdgeData>::Edge & edge = graph.edge(h);
    EXPECT_EQ(edge.a, b);
    EXPECT_EQ(edge.b, d);

    // Iterators
    int count = 0;
    for (auto it = graph.nodeBegin(); it != graph.nodeEnd(); ++it) { count++; }
    ASSERT_EQ(count, 4);
    count = 0;
    for (auto it = graph.edgeBegin(); it != graph.edgeEnd(); ++it) { count++; }
    ASSERT_EQ(count, 4);
    count = 0;
    auto & neighbours = graph.node(b).neighbours;
    for (auto it = neighbours.cbegin(); it != neighbours.cend(); ++it) { count++; }
    ASSERT_EQ(count, 3);

    // Delete edge 'e' (connecting 'a' and 'b')
    graph.deleteEdge(e);
    count = 0;
    for (auto it = graph.nodeBegin(); it != graph.nodeEnd(); it++) { count++; }
    ASSERT_EQ(count, 4);
    count = 0;
    for (auto it = graph.edgeBegin(); it != graph.edgeEnd(); it++) { count++; }
    ASSERT_EQ(count, 3);
    count = 0;
    for (auto it = neighbours.cbegin(); it != neighbours.cend(); it++) { count++; }
    ASSERT_EQ(count, 2);

    // Delete node 'c'
    graph.deleteNode(c);
    count = 0;
    for (auto it = graph.nodeBegin(); it != graph.nodeEnd(); ++it) { count++; }
    ASSERT_EQ(count, 3);
    for (auto it = graph.edgeBegin(); it != graph.edgeEnd(); ++it) { EXPECT_EQ(it->first, h); }
    for (auto it = neighbours.cbegin(); it != neighbours.cend(); ++it) { EXPECT_EQ(*it, h); }

    // Move edge 'h' to connect 'a' and 'b' instead
    EXPECT_EQ(edge.a, b);
    EXPECT_EQ(edge.b, d);
    auto & neighbours2 = graph.node(d).neighbours;
    EXPECT_NE(neighbours2.cbegin(), neighbours2.cend());
    graph.moveEdge(h, a, b);
    EXPECT_EQ(edge.a, a);
    EXPECT_EQ(edge.b, b);
    EXPECT_EQ(neighbours2.cbegin(), neighbours2.cend());
}

TEST(Graph, Dijkstra) {
    /* Test graph:

        9       A
    f-------T-------b
    |\     / \     /|
    | \4  /C  \7  /2|
    |  \ /     \ /  |
    |   e       c   |
    |    \     /    |
   3|    3\   /8    |4
    |      \ /      |
    |       d       |
    |       |       |
    |       |4      |
    |       |       |
    g-------S-------a
        7       5

    Solution: S - a - b - c - T
    */

    struct DijkstraNodeData {
        bool visited = false;
        int weight = 0;
        NodeId backptr;
    };

    struct DijkstraGraph : public Graph<DijkstraNodeData, EdgeData> {
        NodeId backtrace(NodeId id) {
            return node(id).data.backptr;
        }
    };

    class DijkstraComparator {
    public:
        DijkstraComparator(const DijkstraGraph & graph) : graph_(graph) { ; }
        bool operator()(NodeId a, NodeId b) {
            return graph_.node(a).data.weight > graph_.node(b).data.weight;
        }
    private:
        const DijkstraGraph & graph_;
    };

    DijkstraGraph dijkstraGraph;
    DijkstraComparator compare(dijkstraGraph);
    std::vector<NodeId> nodes;
    for (int i = 0; i < 9; i++) {
        nodes.push_back(dijkstraGraph.addNode());
    }
    NodeId S = nodes[7];
    NodeId T = nodes[8];
    dijkstraGraph.addEdge(nodes[S], nodes['a' - 'a'], EdgeData(5));
    dijkstraGraph.addEdge(nodes[S], nodes['d' - 'a'], EdgeData(4));
    dijkstraGraph.addEdge(nodes[S], nodes['g' - 'a'], EdgeData(7));
    dijkstraGraph.addEdge(nodes[T], nodes['b' - 'a'], EdgeData(10));
    dijkstraGraph.addEdge(nodes[T], nodes['c' - 'a'], EdgeData(7));
    dijkstraGraph.addEdge(nodes[T], nodes['e' - 'a'], EdgeData(12));
    dijkstraGraph.addEdge(nodes[T], nodes['f' - 'a'], EdgeData(9));
    dijkstraGraph.addEdge(nodes['b' - 'a'], nodes['a' - 'a'], EdgeData(4));
    dijkstraGraph.addEdge(nodes['b' - 'a'], nodes['c' - 'a'], EdgeData(2));
    dijkstraGraph.addEdge(nodes['c' - 'a'], nodes['d' - 'a'], EdgeData(8));
    dijkstraGraph.addEdge(nodes['e' - 'a'], nodes['d' - 'a'], EdgeData(3));
    dijkstraGraph.addEdge(nodes['f' - 'a'], nodes['e' - 'a'], EdgeData(4));
    dijkstraGraph.addEdge(nodes['f' - 'a'], nodes['g' - 'a'], EdgeData(3));

    std::vector<NodeId> visitedNodes;
    visitedNodes.push_back(S);
    DijkstraNodeData & sData = dijkstraGraph.node(S).data;
    sData.visited = true;
    sData.backptr = S;

    while (visitedNodes.back() != T) {
        NodeId id = visitedNodes.back();
        DijkstraGraph::Node & node = dijkstraGraph.node(id);
        DijkstraGraph::NodeNeighbours & neighbours = node.neighbours;
        DijkstraNodeData & data = node.data;
        visitedNodes.pop_back();
        for (DijkstraGraph::NodeNeighbours::const_iterator it = neighbours.cbegin(); it != neighbours.cend(); ++it) {
            DijkstraGraph::Edge & edge = dijkstraGraph.edge(*it);
            NodeId & nextId = edge.b == id ? edge.a : edge.b;
            DijkstraNodeData & nextData = dijkstraGraph.node(nextId).data;
            int newWeight = data.weight + edge.data.weight;
            if (nextData.visited) {
                if (nextData.weight > newWeight) {
                    nextData.weight = newWeight;
                    nextData.backptr = id;
                }
            } else {
                visitedNodes.push_back(nextId);
                nextData.visited = true;
                nextData.weight = newWeight;
                nextData.backptr = id;
            }
        }
        std::sort(visitedNodes.begin(), visitedNodes.end(), compare);
    }
    NodeId id = dijkstraGraph.backtrace(T);
    EXPECT_EQ(id, nodes['c' - 'a']);
    id = dijkstraGraph.backtrace(id);
    EXPECT_EQ(id, nodes['b' - 'a']);
    id = dijkstraGraph.backtrace(id);
    EXPECT_EQ(id, nodes['a' - 'a']);
    id = dijkstraGraph.backtrace(id);
    EXPECT_EQ(id, S);
}
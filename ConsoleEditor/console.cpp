#include "../editoractions/editoractions.h"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace {
class UnknownCommandException : public std::runtime_error {
public:
    UnknownCommandException(const std::string & message) : std::runtime_error(message) { ; }
};

class RedoFromStartException : public std::exception {};

class MissingArgumentException : public std::exception {};

EditorActions editorActions;
bool quitting = false;

void readNumber(std::istream & stream, int & val) {
    if (stream.eof())
        throw MissingArgumentException();
    stream >> val;
    if (stream.fail())
        throw RedoFromStartException();
}

void readString(std::istream & stream, std::string & s) {
    if (stream.eof())
        throw MissingArgumentException();
    stream >> s;
}

void command_addlocation(std::istream & stream) {
    std::string s;
    if (!stream.eof())
        stream >> s;
    editorActions.addLocation(s);
}

void command_addnode(std::istream & stream) {
    int x, y;
    readNumber(stream, x);
    readNumber(stream, y);
    std::string s;
    if (!stream.eof())
        stream >> s;
    editorActions.addNode(x, y, s);
}

void command_addedge(std::istream & stream) {
    int a, b;
    readNumber(stream, a);
    readNumber(stream, b);
    std::string s;
    if (!stream.eof())
        stream >> s;
    editorActions.addEdge(a, b, s);
}

void command_deletenode(std::istream & stream) {
    int a;
    readNumber(stream, a);
    editorActions.deleteNode(a);
}

void command_deleteedge(std::istream & stream) {
    int e;
    readNumber(stream, e);
    editorActions.deleteEdge(e);
}

void command_movenode(std::istream & stream) {
    int a, x, y;
    readNumber(stream, a);
    readNumber(stream, x);
    readNumber(stream, y);
    editorActions.moveNode(a, x, y);
}

void command_moveedge(std::istream & stream) {
    int e, a, b;
    readNumber(stream, e);
    readNumber(stream, a);
    readNumber(stream, b);
    editorActions.moveEdge(e, a, b);
}

void command_renamenode(std::istream & stream) {
    int a;
    readNumber(stream, a);
    std::string s;
    readString(stream, s);
    editorActions.renameNode(a, s);
}

void command_renameedge(std::istream & stream) {
    int e;
    readNumber(stream, e);
    std::string s;
    readString(stream, s);
    editorActions.renameEdge(e, s);
}

void nodeDump(const MovementGraph::Node & node) {
    const Node & data = node.data;
    std::cout << "Name: " << data.name;
    std::cout << "   x: " << data.x;
    std::cout << "   y: " << data.y << std::endl;
}

void edgeDump(const MovementGraph::Edge & edge) {
    std::cout << "Name: " << edge.data.name;
    std::cout << "   Links: " << edge.a << " and " << edge.b << std::endl;
}

void command_printlocation(std::istream & stream) {
    std::cout << "ID: " << editorActions.currentLocationId();
    std::cout << "   Name: " << editorActions.locationName() << std::endl;
}

void command_printnode(std::istream & stream) {
    int a;
    readNumber(stream, a);
    nodeDump(editorActions.node(a));
}

void command_printedge(std::istream & stream) {
    int e;
    readNumber(stream, e);
    edgeDump(editorActions.edge(e));
}

void command_listnodes(std::istream & stream) {
    for (NodeIterator it = editorActions.nodeBegin(); it != editorActions.nodeEnd(); ++it) {
        std::cout << "ID: " << it->first << "   ";
        nodeDump(it->second);
    }
}

void command_listedges(std::istream & stream) {
    if (stream.eof()) {
        for (EdgeIterator it = editorActions.edgeBegin(); it != editorActions.edgeEnd(); ++it) {
            std::cout << "ID: " << it->first << "   ";
            edgeDump(it->second);
        }
    } else {
        int a;
        readNumber(stream, a);
        const MovementGraph::Node node = editorActions.node(a);
        std::cout << "Edges connected to node " << a << ":";
        auto & neighbours = node.neighbours;
        for (auto it = neighbours.cbegin(); it != neighbours.cend(); ++it) std::cout << " " << *it;
        std::cout << std::endl;
    }
}

void command_newproject(std::istream & stream) {
    editorActions.newProject();
    editorActions.addLocation();
}

void command_undo(std::istream & stream) {
    editorActions.undo();
}

void command_redo(std::istream & stream) {
    editorActions.redo();
}

void command_save(std::istream & stream) {
    std::string file;
    readString(stream, file);
    editorActions.save(file);
}

void command_load(std::istream & stream) {
    std::string file;
    readString(stream, file);
    editorActions.load(file);
}

void command_quit(std::istream &) {
    quitting = true;
}

std::unordered_map<std::string, void(*)(std::istream & stream)> commandMap = {
    {"addlocation", command_addlocation},
    {"printlocation", command_printlocation},
    {"addnode", command_addnode},
    {"addedge", command_addedge},
    {"deletenode", command_deletenode},
    {"deleteedge", command_deleteedge},
    {"movenode", command_movenode},
    {"moveedge", command_moveedge},
    {"renamenode", command_renamenode},
    {"renameedge", command_renameedge},
    {"printnode", command_printnode},
    {"printedge", command_printedge},
    {"listnodes", command_listnodes},
    {"listedges", command_listedges},
    {"newproject", command_newproject},
    {"undo", command_undo},
    {"redo", command_redo},
    {"save", command_save},
    {"load", command_load},
    {"quit", command_quit},
    {"exit", command_quit},
};
}

int main() {
    std::string command;
    std::string line;
    editorActions.addLocation();
    std::cout << "Treasure of Ratsj editor" << std::endl;
    while (!quitting) {
        try {
            std::cout << "> ";
            std::getline(std::cin, line);
            std::istringstream iss(line);
            if (iss.eof())
                throw RedoFromStartException();
            iss >> command;
            auto found = commandMap.find(command);
            if (found == commandMap.end())
                throw UnknownCommandException(command);
            found->second(iss);
        } catch (const UnknownCommandException & ex) {
            std::cout << "Return to sender! Command UNKNOWN! No such COMMAND! No such \"" << ex.what() << "\"!" << std::endl;
        } catch (const MissingArgumentException &) {
            std::cout << "Missing argument" << std::endl;
        } catch (const RedoFromStartException &) {
            std::cout << "Please redo from start" << std::endl;
        } catch (const std::invalid_argument & ex) {
            std::cout << ex.what() << std::endl;
        } catch (const std::out_of_range & ex) {
            std::cout << ex.what() << std::endl;
        } catch (const std::runtime_error & ex) {
            std::cout << ex.what() << std::endl;
        }
    }
    return 0;
}
